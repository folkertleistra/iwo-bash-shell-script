#!/bin/bash
# Author: Folkert Leistra
# Descr: returns all the words: 'de' in a text file
# Date: 15-03-2018
# Usage: ./extract_de.sh FILE
#

TEXT=$1
if [ -z "$TEXT" ]
then
    echo "Please specify a file."
    exit
fi
# tokenize all words and look, grep word 'de', and count the lines for the frequency.

cat $TEXT | grep -Eoh '\w+' | grep -wio 'de' | wc -l